package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int selection;

        while(true){
            System.out.println("SHORT ASSIGNMENT");
            System.out.println("1.Sum Case");
            System.out.println("2.Multiple Case");
            System.out.println("3.QUit");
            System.out.print("Input your selection: ");

            try{
                selection = scanner.nextInt();
                scanner.nextLine();
            }catch (InputMismatchException e){
                scanner.nextLine();
                System.out.println("================================================");
                System.out.println("Please only input number");
                System.out.println("================================================");
                continue;
            }
            switch (selection){
                case 1: {

                    while(true){
                        try{
                            System.out.print("Input your digit :");
                            int digit = scanner.nextInt();
                            scanner.nextLine();
                            SumCase sumCase = new SumCase();
                            int n = sumCase.iterationCount(digit);
                            System.out.println("It takes "+n+" Iterations to reach a single digit number");
                            System.out.println("=========================================================");
                            break;
                        }catch (InputMismatchException e){
                            scanner.nextLine();
                            System.out.println("================================================");
                            System.out.println("Please only input number");
                            System.out.println("================================================");
                            continue;
                        }
                    }
                    break;
                }
                case 2: {
                    while(true){
                        try{
                            System.out.print("Input your digit :");
                            int digit = scanner.nextInt();
                            scanner.nextLine();
                            MultipleCase multipleCase = new MultipleCase();
                            int n = multipleCase.iterationCount(digit);
                            System.out.println("It takes "+n+" Iterations to reach a single digit number");
                            System.out.println("=========================================================");
                            break;
                        }catch (InputMismatchException e){
                            scanner.nextLine();
                            System.out.println("================================================");
                            System.out.println("Please only input number");
                            System.out.println("================================================");
                            continue;
                        }
                    }
                    break;
                }
                case 3: {
                    System.out.println("QUIT!!!");
                    //break the while loop
                    return;
                }
                default:{
                    System.out.println("Please just input from 1 to 3");
                    break;
                }
            }
        }

    }
}
