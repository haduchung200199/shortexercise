package com.company;

/**
 * Class that count iteration of sum case
 */
public class SumCase {
    //n is a number of iterations that take to make "digit" reach a single-digit number
    public int n =0;

    /**
     * count numbers of iterations that take "digit" to reach a single-digit number
     * @param digit
     * @return
     */
    public int iterationCount(int digit){

        int sumOfDigit = sumOfDigits(digit);
        //if the digit is still not the single-digit number -> make another recursion
        if(sumOfDigit>9){
            iterationCount(sumOfDigit);
        }

        return n;
    }

    /**
     * a method that sum the digits of the argument "digits"
     * @param digit
     * @return
     */
    int sumOfDigits(int digit){
        int sumOfDigit = 0;
        //increase the number of iteration if digit is not a single-digit number
        if(digit>9){
            n++;
        }
        //calculate the sum
        while(digit>0){
            sumOfDigit = sumOfDigit + digit%10;
            digit = digit/10;
        }
        return sumOfDigit;
    }



}
