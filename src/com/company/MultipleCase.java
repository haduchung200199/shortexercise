package com.company;

/**
 * class that count iteration of Multiple case
 */
public class MultipleCase {
    //n is a number of iterations that take to make "digit" reach a single-digit number
    int n =0;
    /**
     * count numbers of iterations that take "digit" to reach a single-digit number
     * @param digit
     * @return
     */
    public int iterationCount(int digit){

        int multipleOfDigit = multipleOfDigits(digit);
        //if the digit is still not the single-digit number -> make another recursion
        if(multipleOfDigit >9){
            iterationCount(multipleOfDigit);
        }


        return n;
    }
    /**
     * a method that multiple the digits of the argument "digits"
     * @param digit
     * @return
     */
    int multipleOfDigits(int digit){
        int multipleOfDitgit = 1;
        if(digit>9){
            n++;
        }
        while(digit>0){
            multipleOfDitgit = multipleOfDitgit * (digit%10);
            digit = digit/10;
        }
        return multipleOfDitgit;
    }


}
