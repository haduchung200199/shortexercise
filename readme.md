Create two function that take an Integer as an argument and return n

**1. Sum case:**

digitSum(1679583) ➞ n = 3.
// 1 + 6 + 7 + 9 + 5 + 8 + 3 = 39.
// 3 + 9 = 12.
// 1 + 2 = 3.
// It takes 3 iterations to reach a single-digit number.
digitSum(123456) ➞ n = 2.
// 1 + 2 + 3 + 4 + 5 + 6 = 21.
// 2 + 1 = 3.
digitSum(6) ➞ n = 0.
// Because 6 is already a single-digit integer.

**2. Multiples case**
digitMultiples(77) ➞ n = 4.
// 7 x 7 = 49.
// 4 x 9 = 36.
// 3 x 6 = 18.
// 1 x 8 = 8.
// It takes 4 iterations to reach a single-digit number.
digitMultiples(123456) ➞ n = 2.
// 1 x 2 x 3 x 4 x 5 x 6 = 720.
// 7 x 2 x 0 = 0.
digitMultiples(4) ➞ n = 0.
// Because 4 is already a single-digit integer.

Note the output n is never negative
